package com.hamon.provider.services

import com.hamon.provider.model.ItemMeliResponse
import com.hamon.provider.model.MeliSearchResponse
import com.hamon.provider.util.URLServices
import retrofit2.Response
import retrofit2.http.GET
import retrofit2.http.QueryMap

interface IMeliServices {

    @GET(URLServices.SEARCH)
    suspend fun searchProduct(@QueryMap queryMap: Map<String, String>): Response<MeliSearchResponse>

    @GET(URLServices.ITEM_INFO)
    suspend fun getInfoProduct(@QueryMap queryMap: Map<String, String>): Response<List<ItemMeliResponse>>


}