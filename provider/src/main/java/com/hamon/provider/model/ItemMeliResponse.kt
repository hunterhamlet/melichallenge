package com.hamon.provider.model

import com.google.gson.annotations.SerializedName

data class ItemMeliResponse(
    @SerializedName("code") val code: Int = 0,
    @SerializedName("body") val body: ItemMeli = ItemMeli()
)