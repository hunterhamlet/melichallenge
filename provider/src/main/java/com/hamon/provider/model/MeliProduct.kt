package com.hamon.provider.model

import com.google.gson.annotations.SerializedName
import java.io.Serializable

data class MeliProduct(
    @SerializedName("title") val title: String = "",
    @SerializedName("price") val price: Double = 0.0,
    @SerializedName("id") val id: String = "",
    @SerializedName("thumbnail") val thumbnail: String = "",
    @SerializedName("permalink") val permalink: String = "",
):Serializable
