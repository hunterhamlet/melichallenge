package com.hamon.provider.model

import com.google.gson.annotations.SerializedName

data class ItemMeli(
    @SerializedName("accepts_mercadopago") val acceptsMercadopago: Boolean = false,
    @SerializedName("available_quantity") val availableQuantity: Int = 0,
    @SerializedName("condition") val condition: String = "",
    @SerializedName("id") val id: String = "",
    @SerializedName("permalink") val permalink: String = "",
    @SerializedName("pictures") val pictures: List<Picture> = mutableListOf(),
    @SerializedName("price") val price: Double = 0.0,
    @SerializedName("shipping") val shipping: Shipping = Shipping(),
    @SerializedName("sold_quantity") val soldQuantity: Int = 0,
    @SerializedName("thumbnail") val thumbnail: String = "",
    @SerializedName("title") val title: String = "",
    @SerializedName("warranty") val warranty: String = ""
)

data class Picture(
    @SerializedName("secure_url") val secureUrl: String = ""
)

data class Shipping(
    @SerializedName("free_shipping") val freeShipping: Boolean = false,
    @SerializedName("store_pick_up") val storePickUp: Boolean = false
)