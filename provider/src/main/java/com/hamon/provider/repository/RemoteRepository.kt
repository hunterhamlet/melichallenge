package com.hamon.provider.repository

import com.hamon.provider.API
import com.hamon.provider.actions.APIActions
import com.hamon.provider.model.ItemMeliResponse
import com.hamon.provider.model.MeliSearchResponse
import com.hamon.provider.services.IMeliServices
import com.hamon.provider.util.URLServices
import timber.log.Timber
import java.lang.Exception

class RemoteRepository: RemoteDataSource {

    private val api = API.getServices<IMeliServices>()

    override suspend fun getSearchProduct(query: String, limit: Int): APIActions{
        return try {
            val response = api.searchProduct(
                mapOf(
                    Pair(URLServices.QUERY_SEARCH_KEY, query),
                    Pair(URLServices.QUERY_LIMIT_KEY, limit.toString())
                )
            )
            if (response.isSuccessful) APIActions.OnSuccess(response = response.body() as MeliSearchResponse)
            else APIActions.OnError(message = "Ups, tenemos problemas internos. Intentelo más tarde.")
        } catch (exception: Exception) {
            Timber.e("An error has in RemoteRepository -> fun getSearchProduct")
            exception.printStackTrace()
            APIActions.OnError(message = "Ups, tenemos problemas. Intentelo más tarde.")
        }
    }

    override suspend fun getProductInfo(productId : String): APIActions{
        return try {
            val response = api.getInfoProduct(
                mapOf(
                    Pair(URLServices.QUERY_ID_PRODUCT, productId),
                )
            )
            if (response.isSuccessful){
                val responseCast = response.body() as MutableList<ItemMeliResponse>
                APIActions.OnSuccess(response = responseCast[0] )
            }
            else APIActions.OnError(message = "Ups, tenemos problemas internos. Intentelo más tarde.")
        } catch (exception: Exception) {
            Timber.e("An error has in RemoteRepository -> fun getProductInfo")
            exception.printStackTrace()
            APIActions.OnError(message = "Ups, tenemos problemas. Intentelo más tarde.")
        }
    }

}

interface RemoteDataSource{

    suspend fun getSearchProduct(query: String, limit: Int = 50): APIActions

    suspend fun getProductInfo(productId : String): APIActions

}