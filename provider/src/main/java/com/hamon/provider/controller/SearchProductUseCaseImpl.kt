package com.hamon.provider.controller

import com.hamon.provider.actions.APIActions
import com.hamon.provider.repository.RemoteDataSource
import com.hamon.provider.repository.RemoteRepository

class SearchProductUseCaseImpl(private val remoteDataSource: RemoteDataSource) : SearchProductUseCase {

    override suspend fun invoke(query: String, limit: Int): APIActions =
        remoteDataSource.getSearchProduct(query, limit)

}

interface SearchProductUseCase {
    suspend fun invoke(query: String, limit: Int = 50): APIActions
}