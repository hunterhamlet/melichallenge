package com.hamon.provider.util

object URLServices {
    const val QUERY_SEARCH_KEY = "q"
    const val QUERY_LIMIT_KEY = "limit"
    const val QUERY_ID_PRODUCT = "ids"
    const val SEARCH = "sites/MLM/search"
    const val ITEM_INFO = "/items"
}