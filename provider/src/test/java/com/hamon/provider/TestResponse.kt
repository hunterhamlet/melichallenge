package com.hamon.provider

import com.hamon.provider.model.*

object TestResponse {
    val mockedList = mutableListOf<MeliProduct>(
        MeliProduct(
            title = "Moto G8 Power Lite 64 Gb  Turquesa 4 Gb Ram",
            id = "MLM832826533",
            price = 3799.0,
            thumbnail = "http://http2.mlstatic.com/D_951869-MLA45823378374_052021-I.jpg",
            permalink = "https://www.mercadolibre.com.mx/moto-g8-power-lite-64-gb-turquesa-4-gb-ram/p/MLM15978039"
        ),
        MeliProduct(
            title = "Moto E6 Play 32 Gb Negro 2 Gb Ram",
            id = "MLM871001833",
            price = 2335.99,
            thumbnail = "http://http2.mlstatic.com/D_773160-MLA44508337589_012021-I.jpg",
            permalink = "https://www.mercadolibre.com.mx/moto-e6-play-32-gb-negro-2-gb-ram/p/MLM15243583"
        )
    )

    val mockError = "Ups, tenemos problemas. Intentelo más tarde."

    val mockedItem = ItemMeliResponse(
        code = 200,
        body = ItemMeli(
            true,
            5,
            "new",
            "MLM871001833",
            "https://articulo.mercadolibre.com.mx/MLM-871001833-moto-e6-play-32-gb-negro-2-gb-ram-_JM",
            arrayListOf(
                Picture("https://http2.mlstatic.com/D_773160-MLA44508337589_012021-O.jpg"),
                Picture("https://http2.mlstatic.com/D_773160-MLA44508337589_012021-O.jpg")
            ),
            2335.99,
            Shipping(true, false),
            34,
            "http://http2.mlstatic.com/D_773160-MLA44508337589_012021-I.jpg",
            "Moto E6 Play 32 Gb Negro 2 Gb Ram",
            "30 dias con el fabricante"
        )
    )

}