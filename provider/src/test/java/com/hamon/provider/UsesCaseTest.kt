package com.hamon.provider

import com.hamon.provider.actions.APIActions
import com.hamon.provider.controller.GetItemUseCaseImpl
import com.hamon.provider.controller.SearchProductUseCaseImpl
import com.hamon.provider.repository.RemoteDataSource
import junit.framework.Assert.assertEquals
import kotlinx.coroutines.runBlocking
import org.junit.Before
import org.junit.Test
import org.junit.runner.RunWith
import org.mockito.Mock
import org.mockito.junit.MockitoJUnitRunner
import org.mockito.kotlin.whenever

@RunWith(MockitoJUnitRunner::class)
class UsesCaseTest {

    @Mock
    lateinit var remoteDataSource: RemoteDataSource

    lateinit var searchProductUseCaseImpl: SearchProductUseCaseImpl

    lateinit var getItemUseCaseImpl: GetItemUseCaseImpl

    @Before
    fun setup() {
        searchProductUseCaseImpl = SearchProductUseCaseImpl(remoteDataSource)
        getItemUseCaseImpl = GetItemUseCaseImpl(remoteDataSource)
    }

    @Test
    fun `should success get item list by query`() {
        runBlocking {
            val productSearch = TestResponse.mockedList
            whenever(searchProductUseCaseImpl.invoke(query = "motorola", limit = 2)).thenReturn(
                APIActions.OnSuccess(
                    response = productSearch
                )
            )
            val result = searchProductUseCaseImpl.invoke(
                query = "motorola",
                limit = 2
            ) as APIActions.OnSuccess<*>
            assertEquals(productSearch, result.response)
        }
    }

    @Test
    fun `should error get item list by query`() {
        runBlocking {
            val productSearch = TestResponse.mockError
            whenever(searchProductUseCaseImpl.invoke(query = "motorola", limit = 2)).thenReturn(
                APIActions.OnError(message = TestResponse.mockError)
            )
            val result =
                searchProductUseCaseImpl.invoke(query = "motorola", limit = 2) as APIActions.OnError
            assertEquals(productSearch, result.message)
        }
    }

    @Test
    fun `should success get item by id`(){
        runBlocking {
            val meliProduct = TestResponse.mockedItem
            whenever(getItemUseCaseImpl.invoke("MLM871001833")).thenReturn(APIActions.OnSuccess(response = meliProduct))
            val result = getItemUseCaseImpl.invoke("MLM871001833") as APIActions.OnSuccess<*>
            assertEquals(meliProduct, result.response)
        }
    }

    @Test
    fun `should error get item by id`(){
        runBlocking {
            val errorMock = TestResponse.mockError
            whenever(getItemUseCaseImpl.invoke("MLM871001833")).thenReturn(APIActions.OnError(message = errorMock))
            val result = getItemUseCaseImpl.invoke("MLM871001833") as APIActions.OnError
            assertEquals(errorMock, result.message)
        }
    }


}