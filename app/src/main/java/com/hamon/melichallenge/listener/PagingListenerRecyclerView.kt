package com.hamon.melichallenge.listener

import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView

class PagingListenerRecyclerView(
    private val linearLayoutManager: LinearLayoutManager,
    private val onFinalList: () -> Unit,
) : RecyclerView.OnScrollListener() {

    private var isLoading: Boolean = false
    private var lastVisibleItem: Int = 0
    private var totalItemCount: Int = 0
    private var visibleThreshold = 5

    override fun onScrolled(recyclerView: RecyclerView, dx: Int, dy: Int) {
        super.onScrolled(recyclerView, dx, dy)

        if (dy <= 0) return

        visibleThreshold = linearLayoutManager.findLastVisibleItemPosition()
        totalItemCount = linearLayoutManager.itemCount
        lastVisibleItem = linearLayoutManager.findLastVisibleItemPosition()

        if (!isLoading && totalItemCount <= (lastVisibleItem + visibleThreshold)) {
            onFinalList.invoke()
            isLoading = true
        }

    }

    fun setLoaded() { isLoading = false }

}