package com.hamon.melichallenge.base

import android.os.Bundle
import android.view.View
import android.widget.Toast
import androidx.activity.OnBackPressedCallback
import androidx.fragment.app.Fragment
import androidx.lifecycle.LiveData
import com.hamon.melichallenge.ui.activity.MainActivity
import com.hamon.melichallenge.util.observerOn
import timber.log.Timber

open class BaseFragment : Fragment(), ILogBase, IViewBase, IFragmentBase {

    val parentActivity: MainActivity by lazy {
        activity as MainActivity
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        onBackPress()
    }

    //onBackPress setup
    open fun onBackPressFunction() {}

    private fun onBackPress() {
        requireActivity().onBackPressedDispatcher
            .addCallback(viewLifecycleOwner, object : OnBackPressedCallback(true) {
                override fun handleOnBackPressed() {
                    onBackPressFunction()
                }
            })
    }

    fun finishParentActivity(){
        parentActivity.finish()
    }

    //toast functions
    override fun toast(message: String) {
        Toast.makeText(context, message, Toast.LENGTH_SHORT).show()
    }

    override fun toastLong(message: String) {
        Toast.makeText(context, message, Toast.LENGTH_LONG).show()
    }

    override fun blockUI() {
        parentActivity.blockUI()
    }

    override fun unblockUI() {
        parentActivity.unblockUI()
    }

    override fun showProgressBar() {
        parentActivity.showProgressCard()
    }

    override fun hideProgressBar() {
        parentActivity.hideProgressCard()
    }

    override fun finishActivity() {
        parentActivity.finish()
    }

    override fun workInProgress() {
        parentActivity.apply {
            blockUI()
        }
    }

    override fun workDone() {
        parentActivity.apply {
            unblockUI()
        }
    }

    override fun showSnackBarError(message:String) {
        parentActivity.showSnackBarError(message)
    }

    override fun showSnackBarSuccess(message: String) {
        parentActivity.showSnackBarSuccess(message)
    }

    //logs functions
    override fun logDebug(message: String) {
        Timber.d(message)
    }

    override fun logDebug(tag: String, message: String) {
        Timber.tag(tag).d(message)
    }

    override fun logInfo(message: String) {
        Timber.i(message)
    }

    override fun logInfo(tag: String, message: String) {
        Timber.tag(tag).i(message)
    }

    override fun logWarning(message: String) {
        Timber.w(message)
    }

    override fun logWarning(tag: String, message: String) {
        Timber.tag(tag).w(message)
    }

    override fun logError(message: String) {
        Timber.e(message)
    }

    override fun logError(tag: String, message: String) {
        Timber.tag(tag).e(message)
    }

}