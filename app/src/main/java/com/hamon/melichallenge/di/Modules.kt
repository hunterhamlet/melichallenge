package com.hamon.melichallenge.di

import com.hamon.melichallenge.viewmodel.MainViewModel
import com.hamon.provider.controller.GetItemUseCaseImpl
import com.hamon.provider.controller.SearchProductUseCaseImpl
import com.hamon.provider.repository.RemoteDataSource
import com.hamon.provider.repository.RemoteRepository
import org.koin.androidx.viewmodel.dsl.viewModel
import org.koin.dsl.module

val appModules = module {
    factory <RemoteDataSource>{ RemoteRepository() }
    single { GetItemUseCaseImpl(get()) }
    single { SearchProductUseCaseImpl(get()) }
    viewModel { MainViewModel(get(), get()) }
}