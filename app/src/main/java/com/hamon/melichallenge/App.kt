package com.hamon.melichallenge

import androidx.multidex.BuildConfig
import androidx.multidex.MultiDexApplication
import com.hamon.melichallenge.di.appModules
import org.koin.android.ext.koin.androidContext
import org.koin.android.ext.koin.androidFileProperties
import org.koin.android.ext.koin.androidLogger
import org.koin.core.context.startKoin
import timber.log.Timber

class App : MultiDexApplication() {
    override fun onCreate() {
        super.onCreate()
        if (BuildConfig.DEBUG) {
            Timber.plant(Timber.DebugTree())
        }
        startKoin {
            androidLogger()
            androidContext(this@App)
            androidFileProperties()
            koin.loadModules(
                arrayListOf(
                    appModules
                )
            )
            koin.createRootScope()
        }
    }
}