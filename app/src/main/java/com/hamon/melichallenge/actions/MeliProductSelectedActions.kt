package com.hamon.melichallenge.actions

import com.hamon.provider.model.ItemMeli

sealed class MeliProductSelectedActions {
    object Init: MeliProductSelectedActions()
    data class HasData(val product: ItemMeli) : MeliProductSelectedActions()
    data class HasError(val message: String) : MeliProductSelectedActions()
}