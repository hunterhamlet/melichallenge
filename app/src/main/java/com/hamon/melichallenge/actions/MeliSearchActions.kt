package com.hamon.melichallenge.actions

import com.hamon.provider.model.MeliProduct

sealed class MeliSearchActions {
    object Init: MeliSearchActions()
    data class HasData(val productList: MutableList<MeliProduct>): MeliSearchActions()
    object EmptySearch: MeliSearchActions()
    data class HasError(val message: String): MeliSearchActions()
}