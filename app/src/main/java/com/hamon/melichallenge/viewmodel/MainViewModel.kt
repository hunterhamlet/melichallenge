package com.hamon.melichallenge.viewmodel

import android.content.Context
import android.net.ConnectivityManager
import android.net.Network
import android.os.Build
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.viewModelScope
import com.hamon.melichallenge.actions.MeliProductSelectedActions
import com.hamon.melichallenge.actions.MeliSearchActions
import com.hamon.melichallenge.actions.NetworkActions
import com.hamon.melichallenge.base.BaseViewModel
import com.hamon.provider.API
import com.hamon.provider.actions.APIActions
import com.hamon.provider.controller.GetItemUseCaseImpl
import com.hamon.provider.controller.SearchProductUseCaseImpl
import com.hamon.provider.model.ItemMeli
import com.hamon.provider.model.ItemMeliResponse
import com.hamon.provider.model.MeliSearchResponse
import kotlinx.coroutines.launch

class MainViewModel(
    private val searchProductUseCaseImpl: SearchProductUseCaseImpl,
    private val getItemUseCaseImpl: GetItemUseCaseImpl
) : BaseViewModel() {

    private val _networkActions: MutableLiveData<NetworkActions> by lazy {
        MutableLiveData<NetworkActions>().apply { value = NetworkActions.Init }
    }
    val networkActions: LiveData<NetworkActions> get() = _networkActions
    private val _searchActions: MutableLiveData<MeliSearchActions> by lazy {
        MutableLiveData<MeliSearchActions>().apply { value = MeliSearchActions.Init }
    }
    val searchActions: LiveData<MeliSearchActions> get() = _searchActions
    private val _getItemActions: MutableLiveData<MeliProductSelectedActions> by lazy {
        MutableLiveData<MeliProductSelectedActions>().apply {
            value = MeliProductSelectedActions.Init
        }
    }
    val getItemActions: LiveData<MeliProductSelectedActions> get() = _getItemActions
    private val _searchQuery: MutableLiveData<String> by lazy {
        MutableLiveData<String>().apply { value = "" }
    }
    private val _productItemSelectedId: MutableLiveData<String> by lazy {
        MutableLiveData<String>().apply { value = "" }
    }


    fun searchProduct() {
        _searchQuery.value?.let { query ->
            viewModelScope.launch {
                when (val result = searchProductUseCaseImpl.invoke(query)) {
                    is APIActions.OnSuccess<*> -> checkData(result.response as MeliSearchResponse)
                    is APIActions.OnError -> _searchActions.postValue(
                        (MeliSearchActions.HasError(
                            result.message
                        ))
                    )
                }
            }
        }
    }

    private fun checkData(data: MeliSearchResponse) {
        if (data.results.isEmpty()) {
            _searchActions.postValue(
                MeliSearchActions.EmptySearch
            )
        } else {
            _searchActions.postValue(
                MeliSearchActions.HasData(
                    data.results,
                )
            )
        }
    }

    fun setQuery(query: String) {
        _searchQuery.value = query
    }

    fun setProductItemSelected(productId: String) {
        _productItemSelectedId.value = productId
    }

    fun getProductInfo() {
        viewModelScope.launch {
            _productItemSelectedId.value?.let {
                when(val result = getItemUseCaseImpl.invoke(it)){
                    is APIActions.OnSuccess<*> -> checkInfoData(result.response as ItemMeliResponse)
                    is APIActions.OnError -> _getItemActions.postValue(MeliProductSelectedActions.HasError(result.message))
                }
            }
        }
    }

    private fun checkInfoData(itemMeli: ItemMeliResponse){
        _getItemActions.postValue(MeliProductSelectedActions.HasData(itemMeli.body))
    }

    fun restartNetworkEvent() {
        _networkActions.value = NetworkActions.Init
    }

    fun isNetworkConnected(context: Context) {
        val connectivityManager =
            context.getSystemService(Context.CONNECTIVITY_SERVICE) as ConnectivityManager
        if (Build.VERSION.SDK_INT <= Build.VERSION_CODES.P) {
            checkConnectivityInAndroidPOrBelow(connectivityManager)
        } else {
            checkConnectivityInAndroidPOrHigher(connectivityManager)
        }
    }

    private fun checkConnectivityInAndroidPOrBelow(connectivityManager: ConnectivityManager) {
        if (connectivityManager.activeNetworkInfo?.isConnectedOrConnecting == true) {
            _networkActions.postValue(NetworkActions.NetworkOK)
        } else {
            _networkActions.postValue(NetworkActions.NetworkError)
        }
    }

    private fun checkConnectivityInAndroidPOrHigher(connectivityManager: ConnectivityManager) {
        connectivityManager.registerDefaultNetworkCallback(object :
            ConnectivityManager.NetworkCallback() {
            override fun onAvailable(network: Network) {
                super.onAvailable(network)
                _networkActions.postValue(NetworkActions.NetworkOK)
            }

            override fun onLost(network: Network) {
                super.onLost(network)
                _networkActions.postValue(NetworkActions.NetworkError)
            }
        })
    }

}