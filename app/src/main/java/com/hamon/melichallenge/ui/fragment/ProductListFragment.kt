package com.hamon.melichallenge.ui.fragment

import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.core.os.bundleOf
import androidx.navigation.fragment.findNavController
import androidx.recyclerview.widget.LinearLayoutManager
import coil.load
import com.hamon.melichallenge.R
import com.hamon.melichallenge.actions.MeliSearchActions
import com.hamon.melichallenge.actions.NetworkActions
import com.hamon.melichallenge.base.BaseFragment
import com.hamon.melichallenge.databinding.FragmentProductListBinding
import com.hamon.melichallenge.listener.PagingListenerRecyclerView
import com.hamon.melichallenge.ui.adapter.ProductAdapter
import com.hamon.melichallenge.viewmodel.MainViewModel
import com.hamon.provider.model.MeliProduct
import org.koin.androidx.viewmodel.ext.android.sharedViewModel

class ProductListFragment : BaseFragment() {

    private val binding: FragmentProductListBinding by lazy {
        FragmentProductListBinding.inflate(LayoutInflater.from(context), null, false)
    }
    private val viewModel: MainViewModel by sharedViewModel()
    private val productAdapter: ProductAdapter by lazy {
        ProductAdapter { product ->
            onClickProduct(product.id)
        }
    }
    private var recyclerViewListener: PagingListenerRecyclerView? = null

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View = binding.root

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        hideProgressBar()
        setupRecyclerview()
        setupObservable()
    }

    override fun onBackPressFunction() {
        super.onBackPressFunction()
        findNavController().navigateUp()
    }

    private fun onClickProduct(productId: String) {
        context?.let { viewModel.isNetworkConnected(it) }
        viewModel.setProductItemSelected(productId)
        showProgressBar()
    }

    private fun setupRecyclerview() {
        binding.rvProducts.apply {
            adapter = productAdapter
            recyclerViewListener =
                PagingListenerRecyclerView(layoutManager as LinearLayoutManager) {
                    productAdapter.addLoading()
                    viewModel.searchProduct()
                }
            recyclerViewListener?.let { addOnScrollListener(it) }
        }
    }

    private fun setupObservable() {
        viewModel.apply {
            searchActions.observe(viewLifecycleOwner) { action ->
                when (action) {
                    is MeliSearchActions.HasData -> addListInAdapter(
                        action.productList
                    )
                }
            }
            networkActions.observe(viewLifecycleOwner){ event ->
                when(event){
                    is NetworkActions.NetworkOK -> moveToDetailsProduct()
                    is NetworkActions.NetworkError -> showProblemNetwork()
                }
            }
        }
    }

    private fun moveToDetailsProduct(){
        viewModel.restartNetworkEvent()
        findNavController().navigate(R.id.fragment_product_detail,)
    }

    private fun addListInAdapter(list: MutableList<MeliProduct>) {
        recyclerViewListener?.setLoaded()
        productAdapter.removeLoading()
        productAdapter.addProducts(list)
        hideInformativeScreen()
    }

    private fun hideInformativeScreen() {
        binding.apply {
            cInformativeScreen.visibility = View.GONE
            rvProducts.visibility = View.VISIBLE
        }
    }

    private fun showProblemNetwork() {
        hideProgressBar()
        binding.apply {
            ivInformativeIcon.load(R.drawable.ic_no_wifi)
            tvInformativeText.text = getString(R.string.network_error)
            cInformativeScreen.visibility = View.VISIBLE
        }
        viewModel.restartNetworkEvent()
    }

}