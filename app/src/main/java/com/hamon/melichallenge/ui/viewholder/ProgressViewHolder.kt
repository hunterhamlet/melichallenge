package com.hamon.melichallenge.ui.viewholder

import androidx.recyclerview.widget.RecyclerView
import com.hamon.melichallenge.databinding.LayoutPagingLoadingBinding

class ProgressViewHolder(binding: LayoutPagingLoadingBinding) :
    RecyclerView.ViewHolder(binding.root) {

}