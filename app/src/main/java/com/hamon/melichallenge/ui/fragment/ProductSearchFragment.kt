package com.hamon.melichallenge.ui.fragment

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.appcompat.widget.SearchView
import androidx.core.os.bundleOf
import androidx.navigation.fragment.findNavController
import androidx.recyclerview.widget.LinearLayoutManager
import coil.load
import com.hamon.melichallenge.actions.MeliSearchActions
import com.hamon.melichallenge.R
import com.hamon.melichallenge.actions.NetworkActions
import com.hamon.melichallenge.base.BaseFragment
import com.hamon.melichallenge.databinding.FragmentProductSearchBinding
import com.hamon.melichallenge.listener.PagingListenerRecyclerView
import com.hamon.melichallenge.ui.adapter.ProductAdapter
import com.hamon.melichallenge.util.hideKeyboard
import com.hamon.melichallenge.viewmodel.MainViewModel
import com.hamon.provider.model.MeliProduct
import org.koin.androidx.viewmodel.ext.android.sharedViewModel

class ProductSearchFragment : BaseFragment() {

    private val binding: FragmentProductSearchBinding by lazy {
        FragmentProductSearchBinding.inflate(LayoutInflater.from(context), null, false)
    }
    private val viewModel: MainViewModel by sharedViewModel()

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View = binding.root

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        setupSearchView()
        setupObservable()
    }

    override fun onBackPressFunction() {
        super.onBackPressFunction()
        finishParentActivity()
    }

    private fun setupSearchView() {
        binding.svProduct.setOnQueryTextListener(object : SearchView.OnQueryTextListener {

            override fun onQueryTextSubmit(query: String?): Boolean {
                hideKeyboard()
                if (!query.isNullOrEmpty()) {
                    viewModel.apply {
                        setQuery(query)
                        context?.let { isNetworkConnected(it) }
                    }
                    showProgressBar()
                }
                return false
            }

            override fun onQueryTextChange(newText: String?): Boolean = false

        })

    }

    private fun hasAnError(message: String) {
        hideProgressBar()
        showSnackBarError(message)
    }

    private fun setupObservable() {
        viewModel.apply {
            searchActions.observe(viewLifecycleOwner) { event ->
                when (event) {
                    is MeliSearchActions.HasData -> navigateToList()
                    is MeliSearchActions.HasError -> hasAnError(event.message)
                    is MeliSearchActions.EmptySearch -> showEmptyList()
                }
            }
            networkActions.observe(viewLifecycleOwner){ event ->
                when(event){
                    is NetworkActions.NetworkOK -> viewModel.searchProduct()
                    is NetworkActions.NetworkError -> showProblemNetwork()
                }
            }
        }
    }

    private fun navigateToList() {
        viewModel.restartNetworkEvent()
        findNavController().navigate(R.id.fragment_product_list)
    }

    private fun showEmptyList() {
        hideProgressBar()
        binding.apply {
            ivInformativeIcon.load(R.drawable.ic_to_do_list)
            tvInformativeText.text = getString(R.string.empty_search_product)
            cInformativeScreen.visibility = View.VISIBLE
        }
    }

    private fun showProblemNetwork() {
        hideProgressBar()
        binding.apply {
            ivInformativeIcon.load(R.drawable.ic_no_wifi)
            tvInformativeText.text = getString(R.string.network_error)
            cInformativeScreen.visibility = View.VISIBLE
        }
        viewModel.restartNetworkEvent()
    }

}