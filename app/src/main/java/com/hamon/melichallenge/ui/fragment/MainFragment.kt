package com.hamon.melichallenge.ui.fragment

import androidx.lifecycle.ViewModelProvider
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.hamon.melichallenge.base.BaseFragment
import com.hamon.melichallenge.databinding.MainFragmentBinding
import com.hamon.melichallenge.viewmodel.MainViewModel

class MainFragment : BaseFragment() {

    private val binding: MainFragmentBinding by lazy {
        MainFragmentBinding.inflate(LayoutInflater.from(context), null, false)
    }

    companion object {
        fun newInstance() = MainFragment()
    }

    private lateinit var viewModel: MainViewModel

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?,
                              savedInstanceState: Bundle?): View {
        return binding.root
    }

    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)
        viewModel = ViewModelProvider(this).get(MainViewModel::class.java)
        // TODO: Use the ViewModel
    }

}