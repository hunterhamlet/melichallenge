package com.hamon.melichallenge.ui.adapter

import androidx.recyclerview.widget.DiffUtil
import com.hamon.provider.model.MeliProduct

class MeliProductDiffUtilCallBack: DiffUtil.ItemCallback<MeliProduct>() {

    override fun areItemsTheSame(oldItem: MeliProduct, newItem: MeliProduct): Boolean {
        return oldItem.id == newItem.id
    }

    override fun areContentsTheSame(oldItem: MeliProduct, newItem: MeliProduct): Boolean {
        return oldItem == newItem
    }
}