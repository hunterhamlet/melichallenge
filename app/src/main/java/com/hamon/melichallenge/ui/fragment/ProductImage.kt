package com.hamon.melichallenge.ui.fragment

import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import coil.load
import com.hamon.melichallenge.databinding.LayoutImageProductBinding

private const val URL_IMAGE = "url_image"

class ProductImage : Fragment() {

    private var urlImage: String? = null
    private val binding: LayoutImageProductBinding by lazy {
        LayoutImageProductBinding.inflate(LayoutInflater.from(context), null, false)
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        arguments?.let {
            urlImage = it.getString(URL_IMAGE)
        }
    }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View = binding.root

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        binding.ivProductThumbnail.load(urlImage)
    }

    companion object {
        @JvmStatic
        fun newInstance(urlImage: String) =
            ProductImage().apply {
                arguments = Bundle().apply {
                    putString(URL_IMAGE, urlImage)
                }
            }
    }
}