package com.hamon.melichallenge.ui.viewholder

import androidx.recyclerview.widget.RecyclerView
import coil.load
import com.hamon.melichallenge.R
import com.hamon.melichallenge.databinding.LayoutItemMeliProductBinding
import com.hamon.provider.model.MeliProduct

class MeliProductViewHolder(private val binding: LayoutItemMeliProductBinding) :
    RecyclerView.ViewHolder(binding.root) {

    fun bind(product: MeliProduct, onTapProduct: (product: MeliProduct) -> Unit) {
        binding.apply {
            ivImageProduct.load(product.thumbnail)
            tvNameProduct.text = product.title
            cProduct.setOnClickListener { onTapProduct.invoke(product) }
            tvPriceProduct.text =
                binding.root.context.getString(R.string.price_product, product.price.toString())
        }
    }

}